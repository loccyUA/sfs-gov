package com.sfs.test;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import com.pages.DeclaratePO;
import com.pages.MainPage;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testcontainers.containers.BrowserWebDriverContainer;
import org.testng.annotations.BeforeClass;

public class BaseTest {
    @BeforeClass
    public void init() {


       System.setProperty("webdriver.chrome.driver", "..//SFS.GOV.ua/chromedriver.exe");
       System.setProperty("selenide.browser", "Chrome");
        ChromeOptions option = new ChromeOptions();
        option.addArguments("--window-size=1920,1080");
        driver = new ChromeDriver(option);


    }

    WebDriver driver;
    public WebDriverWait wait;


    MainPage mainPage = new MainPage(driver);
    DeclaratePO declaratePO = new DeclaratePO(driver);




}
