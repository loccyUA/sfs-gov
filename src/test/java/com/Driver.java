package com;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Driver {
    private static WebDriver driver = null;
    public  Driver() {
        System.out.println("Drivers initiated");
    }

    public WebDriver getChromeDriver(){
        System.setProperty("webdriver.chrome.driver", "c:/drivers/chromedriver.exe");

        return driver = new ChromeDriver();
    }
    public void closeDriver() {
        if (driver != null) {
            driver.close();
        }
    }
}
