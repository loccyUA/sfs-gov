package com.pages;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.WebDriver;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.*;

public class DeclaratePO {
    public WebDriver driver;
    public DeclaratePO(WebDriver driver) {this.driver = driver;}

    //1
    // Тип декларації:
    private String zvitna_1 = "#HZ";
    private String zvitnaNova_1 = "#HZN";
    private String Uto4n_1 = "#HZU";

    //2
    // Звітний (податковий) період:
    private String Zv_P_2 = "#HZY";
    //Звітний (податковий) період, що уточнюється:
    private String Zv_U_2 = "#HZYP";


    //3
    //Прізвище, ім'я, по батькові платника податку:
    private String P_I_B_3 = "#HNAME";
    //Реєстраційний номер облікової картки:
    private String Reg_Nomer_3 = "#HTIN";

    //4
    //Податкова адреса (місце проживання) платника податку:
    //Лівий блок
    private String Obl_4 = "#HREG";
    private String Ray_4 = "#HRAJ";
    private String Misto_4 = "#HCITY";
    private String Vulica_4 = "#HSTREET";
    private String Nomer_Bud_4 = "#HBUILD";
    private String Korpus_4 = "#HCORP";
    private String Nomer_Kv_4 = "#HAPT";
    //Правий блок
    private String Posht_Idx_4 = "#HZIP";
    private String Tel_4 = "#HTEL";
    private String Email_4 = "#HEMAIL";

    //5
    //Найменування контролюючого органу, до якого подається декларація:
    private String Kontrol_Org_5 = "#HSTI";

    //6
    //Резидентський статус платника податку
    private String Rez_6 = "#H01";
    private String NRez_6 = "#H02";

    //7
    //Декларація заповнена:
    private String Sam_PL = "#H03";
    private String Upovn_Os = "#H04";

    //8
    //Категорія платника:
    private String Gromod = "#H5";
    private String Prof_Os = "#H6";

    //9
    //Прізвище, ім'я, по батькові уповноваженої особи:
    private String P_I_B_9 = "#HNAMEAG";
    //Реєстраційний номер облікової картки платника
    private String Reg_Nomer_9 = "#HTINAG";

    //10
    //ДОХОДИ, ЯКІ ВКЛЮЧАЮТЬСЯ ДО ЗАГАЛЬНОГО РІЧНОГО ОПОДАТКОВУВАНОГО ДОХОДУ
    //10.1
    private String Sum_10_1 = "#R0101G3";
    private String Pod_10_1 = "#R0101G4";
    private String War_10_1 = "#R0101G5";
    //10.2
    private String Sum_10_2 = "#R0102G3";
    private String Pod_10_2 = "#R0102G4";
    private String War_10_2 = "#R0102G5";
    //10.3
    private String Sum_10_3 = "#R0103G3";
    private String Pod_10_3 = "#R0103G4";
    private String War_10_3 = "#R0103G5";
    //10.4
    private String Sum_10_4 = "#R0104G3";
    private String Pod_10_4 = "#R0104G4";
    private String War_10_4 = "#R0104G5";
    //10.5
    private String Sum_10_5 = "#R0105G3";
    private String Pod_10_5 = "#R0105G4";
    private String War_10_5 = "#R0105G5";
    //10.6
    private String Sum_10_6 = "#R0106G3";
    //10.7
    private String Sum_10_7 = "#R0107G3";
    //10.8
    private String Sum_10_8 = "#R0108G3";
    //10.9
    private String Sum_10_9 = "#R0109G3";
    private String Pod_10_9 = "#R0109G4";
    private String War_10_9 = "#R0109G5";

    //11
    //ДОХОДИ, ЯКІ НЕ ВКЛЮЧАЮТЬСЯ ДО ЗАГАЛЬНОГО РІЧНОГО ОПОДАТКОВУВАНОГО ДОХОДУ

    //11.1
    //Доходи, отримані від провадження господарської діяльності
    private String Doh__gos_11_1 = "#R0111G3";
    //11.2
    //Доходи від операцій з продажу (обміну) об'єктів
    private String Doh__oper_11_2 = "#R0112G3";
    //11.3
    //Інші доходи, що не підлягають оподаткуванню
    private String In_doh_11_3 = "#R0113G3";

    //РОЗРАХУНОК СУМИ ПОДАТКУ, НА ЯКУ ЗМЕНШУЮТЬСЯ ПОДАТКОВІ ЗОБОВ'ЯЗАННЯ
    //13
    private String Kat_Vitrat_13_0_1 = "#R013G1";
    private String Kat_Vitrat_13_0_2 = "#R013G2";
    private String Kat_Vitrat_13_0_3 = "#R013G3";
    private String Kat_Vitrat_13_0_4 = "#R013G4";
    private String Kat_Vitrat_13_0_5 = "#R013G5";
    private String Kat_Vitrat_13_0_6 = "#R013G6";
    private String Kat_Vitrat_13_0_7 = "#R013G7";

    //14
    //Загальна сума фактично здійснених протягом звітного
    private String Sum_14 = "#R014G3";

    //15
    //Сума нарахованої заробітної плати
    private String Sum_15 = "#R015G3";

    //16
    //Сума нарахованої заробітної плати
    private String Sum_16 = "#R016G3";

    //17
    //Сума нарахованої заробітної плати
    private String Sum_17 = "#R017G3";

    //ПОДАТКОВІ ЗОБОВ'ЯЗАННЯ З ПОДАТКУ НА ДОХОДИ ФІЗИЧНИХ ОСІБ / ВІЙСЬКОВОГО ЗБОРУ
    //18
    //Загальна сума податкових зобов'язань
    private String Zag_Sum_18 = "#R018G3";

    //19
    //Сума податку, на яку зменшуються податкові зобов'язання з податку на доходи фізичних осіб у зв'язку з використанням права на податкову знижку
    private String Sum_19 = "#R019G3";

    //20
    //Сума податку на доходи фізичних осіб, що підлягає поверненню у разі невчинення нотаріальної дії щодо посвідчення договору купівлі-продажу
    private String Sum_20 = "#R020G3";

    //21
    //Сума податків, сплачених за кордоном, на яку зменшується сума річного податкового зобов'язання згідно з підпунктом 170.11.2
    private String Sum_21 = "#R021G3";

    //22.1
    //Сума податкових зобов'язань з податку на доходи фізичних осіб, що підлягає сплаті до бюджету самостійно платником податків
    private String Sum_22_1 = "#R0221G3";

    //22.2
    //Сума податку на доходи фізичних осіб, що підлягає поверненню з бюджету за результатами звітного
    private String Sum_22_2 = "#R0222G3";

    //23.1
    //Сума податкових зобов'язань з військового збору, що підлягає сплаті до бюджету самостійно платником податків за результатами звітного (податкового) року
    private String Sum_23_1 = "#R0231G3";

    //РОЗРАХУНОК ПОДАТКОВИХ ЗОБОВ'ЯЗАНЬ У ЗВ'ЯЗКУ З ВИПРАВЛЕННЯМ САМОСТІЙНО ВИЯВЛЕНИХ ПОМИЛОК У ПОПЕРЕДНІХ ЗВІТНИХ ПЕРІОДАХ
    //24
    //Сума податку та/або збору, які підлягали перерахуванню до бюджету або поверненню, за даними звітного (податкового) періоду
    private String Sum_Pod_24 = "#R024G3";
    private String Sum_War_24 = "#R024G4";
    //25
    //Сума податку та/або збору, які підлягали перерахуванню до бюджету або поверненню, за даними звітного (податкового) періоду
    private String Sum_Pod_25 = "#R025G3";
    private String Sum_War_25 = "#R025G4";
    //26.1
    //Сума податку та/або збору, які підлягали перерахуванню до бюджету або поверненню, за даними звітного (податкового) періоду
    private String Sum_Pod_26_1 = "#R0261G3";
    private String Sum_War_26_1 = "#R0261G4";
    //26.2
    //Сума податку та/або збору, які підлягали перерахуванню до бюджету або поверненню, за даними звітного (податкового) періоду
    private String Sum_Pod_26_2 = "#R0262G3";
    private String Sum_War_26_2 = "#R0262G4";
    //27
    //Сума податку та/або збору, які підлягали перерахуванню до бюджету або поверненню, за даними звітного (податкового) періоду
    private String Sum_PROC = "#0_R027G2";
    private String Sum_Pod_27 = "#R027G3";
    private String Sum_War_27 = "#R027G4";
    //28
    //Сума податку та/або збору, які підлягали перерахуванню до бюджету або поверненню, за даними звітного (податкового) періоду
    private String Sum_Pod_28 = "#R028G3";
    private String Sum_War_28 = "#R028G4";

    //Реквізити банківського рахунку для перерахування коштів у разі повернення надміру утриманих (сплачених) сум податку
    //29
    private String Nom_rah_29 = "#HBANKACC";
    private String Bank_29 = "#HBANKNAME";
    private String MFO_29 = "#HMFO";

    //ВІДОМОСТІ ПРО ВЛАСНЕ НЕРУХОМЕ (РУХОМЕ) МАЙНО ТА/АБО МАЙНО, ЯКЕ НАДАЄТЬСЯ В ОРЕНДУ (СУБОРЕНДУ)
    //Категорії об'єктів***
    private String Kat_Ob = "#748935b5-71ca-493e-1830-4f822deafa5e_T1RXXXXG2";
    //Місцезнаходження об'єкта нерухомого майна (країна, адреса) або марка (модель) рухомого майна
    private String Misce_Ob = "#748935b5-71ca-493e-1830-4f822deafa5e_T1RXXXXG3S";
    //Рік набуття у власність/ рік випуску (для рухомого майна)
    private String Rik = "#748935b5-71ca-493e-1830-4f822deafa5e_T1RXXXXG4";
    //Загальна площа нерухомого майна
    private String Zag_Plosha = "#748935b5-71ca-493e-1830-4f822deafa5e_T1RXXXXG5";
    //Частка в загальній площі нерухомого майна (десятковий дріб)
    private String Chastka_Ploshi = "#748935b5-71ca-493e-1830-4f822deafa5e_T1RXXXXG6";
    //Відмітка про надання майна в оренду (суборенду), житловий найм (піднайм)
    private String Vidmitka = "#748935b5-71ca-493e-1830-4f822deafa5e_T1RXXXXG7";

    //Наявність доповнення
    private String Nayavnist_Dop = "#HJ1";

    //Доповнення до податкової декларації
    //Зміст доповнення
    private String Zmist_dop = "#0ba7d6da-e37e-45ed-5c52-059a166cdb3d_T2RXXXXG2S";
    //Додатки до декларації (потрібне позначити):
    //Ф1
    private String F1 = "#HD1";
    //Ф2
    private String F2 = "#HD2";

    //Дата подання декларації
    private String Data = "#HFILL";

    //Фізична особа - платник податку або уповноважена особа (footer)
    //(ініціали та прізвище)
    private String INI = "#HBOS";






    public void switchTs() {
        switchTo().frame($(byXpath("//iframe[contains(@src,'/cabinet/servlet/')]")));
    }
    public void set_3(String text){
        $((P_I_B_3)).click();
        $((P_I_B_3 + " input")).sendKeys(text);

    }

    public void set_3Reg(String text){
        $((Reg_Nomer_3)).scrollTo();
        $((Reg_Nomer_3)).click();
        $((Reg_Nomer_3 + " input")).sendKeys(text);
    }

    public void set_4_Obl(String text){
        $((Obl_4)).click();
        $((Obl_4 + " input")).sendKeys(text);
    }

    public void set_4_Disctrick(String text){
        $((Ray_4)).click();
        $((Ray_4 + " input")).sendKeys(text);
    }

    public void set_4_City(String text){
        $((Misto_4)).click();
        $((Misto_4 + " input")).sendKeys(text);}


    public void set_4_Street(String text){
        $((Vulica_4)).click();
        $((Vulica_4 + " input")).sendKeys(text);
    }

    public void set_4_Build(String text){
        $((Nomer_Bud_4)).click();
        $((Nomer_Bud_4 + " input")).sendKeys(text);
    }

    public void set_4_Apt(String text){$((Nomer_Kv_4)).sendKeys(text);}
    public void set_4_Ind(String text){$((Posht_Idx_4)).sendKeys(text);}
    public void set_4_Tel(String text){$((Tel_4)).sendKeys(text);}
    public void set_4_Email(String text){$((Email_4)).sendKeys(text);}


    public void set_5(String text){
        $((Kontrol_Org_5)).click();
        $((Kontrol_Org_5 + " input")).sendKeys(text);
    }

    public void click_6(){
        $((Rez_6)).scrollTo();
        $((Rez_6)).click();
    }

    public void click_7(){$((Sam_PL)).click();}

    public void click_8(){
        $((Gromod)).scrollTo();
        $((Gromod)).click();}

    public void set_9(String text){$((P_I_B_9)).sendKeys(text);}
    public void set_9Reg(String text){$((Reg_Nomer_9)).sendKeys(text);}


    public void set_10_1_Sum(String text){$((Sum_10_1)).sendKeys(text);}
    public void set_10_1_MoneyFisOs(String text){$((Pod_10_1)).sendKeys(text);}
    public void set_10_1_MoneyWar(String text){$((War_10_1)).sendKeys(text);}
    public void set_10_2_Sum(String text){$((Sum_10_2)).sendKeys(text);}
    public void set_10_2_MoneyFisOs(String text){$((Pod_10_2)).sendKeys(text);}
    public void set_10_2_MoneyWar(String text){$((War_10_2)).sendKeys(text);}
    public void set_10_3_Sum(String text){$((Sum_10_3)).sendKeys(text);}
    public void set_10_3_MoneyFisOs(String text){$((Pod_10_3)).sendKeys(text);}
    public void set_10_3_MoneyWar(String text){$((War_10_3)).sendKeys(text);}
    public void set_10_4_Sum(String text){$((Sum_10_4)).sendKeys(text);}
    public void set_10_4_MoneyFisOs(String text){$((Pod_10_4)).sendKeys(text);}
    public void set_10_4_MoneyWar(String text){$((War_10_4)).sendKeys(text);}
    public void set_10_5_Sum(String text){$((Sum_10_5)).sendKeys(text);}
    public void set_10_5_MoneyFisOs(String text){$((Pod_10_5)).sendKeys(text);}
    public void set_10_5_MoneyWar(String text){$((War_10_5)).sendKeys(text);}
    public void set_10_6_Sum(String text){$((Sum_10_6)).sendKeys(text);}
    public void set_10_7_Sum(String text){$((Sum_10_7)).sendKeys(text);}
    public void set_10_8_Sum(String text){$((Sum_10_8)).sendKeys(text);}
    public void set_10_9_Sum(String text){$((Sum_10_9)).sendKeys(text);}
    public void set_10_9_MoneyFisOs(String text){$((Pod_10_9)).sendKeys(text);}
    public void set_10_9_MoneyWar(String text){$((War_10_9)).sendKeys(text);}


    public void set_11_1(String text){$((Doh__gos_11_1)).sendKeys(text);}
    public void set_11_2(String text){$((Doh__oper_11_2)).sendKeys(text);}
    public void set_11_3(String text){$((In_doh_11_3)).sendKeys(text);}

    public void set_14(String text){$((Sum_14)).sendKeys(text);}
    public void set_15(String text){$((Sum_15)).sendKeys(text);}
    public void set_16(String text){$((Sum_16)).sendKeys(text);}

    public void set_18(String text){$((Zag_Sum_18)).sendKeys(text);}
    public void set_20(String text){$((Sum_20)).sendKeys(text);}
    public void set_21(String text){$((Sum_21)).sendKeys(text);}
    public void set_22_1(String text){$((Sum_22_1)).sendKeys(text);}
    public void set_23_1(String text){$((Sum_23_1)).sendKeys(text);}

    public void set_24(String text){$((Sum_Pod_24)).sendKeys(text);}
    public void set_24W(String text){$((Sum_War_24)).sendKeys(text);}
    public void set_25(String text){$((Sum_Pod_25)).sendKeys(text);}
    public void set_25W(String text){$((Sum_War_25)).sendKeys(text);}
    public void set_26_1(String text){$((Sum_Pod_26_1)).sendKeys(text);}
    public void set_26_1W(String text){$((Sum_War_26_1)).sendKeys(text);}
    public void set_26_2(String text){$((Sum_Pod_26_2)).sendKeys(text);}
    public void set_26_2W(String text){$((Sum_War_26_2)).sendKeys(text);}
    public void set_27(String text){$((Sum_Pod_27)).sendKeys(text);}
    public void set_27_Proc(String text){$((Sum_PROC)).sendKeys(text);}
    public void set_27W(String text){$((Sum_War_27)).sendKeys(text);}
    public void set_28(String text){$((Sum_Pod_28)).sendKeys(text);}
    public void set_28W(String text){$((Sum_War_28)).sendKeys(text);}

    public void set_29Rah(String text){$((Nom_rah_29)).sendKeys(text);}
    public void set_29Bank(String text){$((Bank_29)).sendKeys(text);}
    public void set_29MFO(String text){$((MFO_29)).sendKeys(text);}


    public void set_INICIALY(String text){
        $((INI)).scrollTo();
        $((INI)).click();
        $((INI + " input")).sendKeys(text);
    }


}
